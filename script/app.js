// MODULE
var angularApp = angular.module('angularApp', ['ngMessages','ngRoute']);

angularApp.config(['$routeProvider', '$locationProvider', function ($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'basic.html',
            controller: 'basicController'
        })
        .when('/rest', {
            templateUrl: 'rest.html',
            controller: 'restController'
        })

}]);



// CONTROLLERS

angularApp.controller('basicController',['$scope',function ($scope) {
    $scope.nameB = '';
    $scope.maxChar = 5;
}]);

angularApp.controller('restController',['$scope','$http',function ($scope, $http) {

    $http({
        method: 'GET',
        url: '/notes'
    }).then(function successCallback(response) {

        $scope.itens = response.data;

    }, function errorCallback(response) {

        console.log(response)

    });

    $scope.newItemTitle = '';
    $scope.newItemContent = '';

    $scope.addItem = function () {
        $http({
            method:'POST',
            url:'/notes',
            data: {title: $scope.newItemTitle, content:$scope.newItemContent}
        }).then(function successCallBack(response){
            console.log('SALVO!');

            $scope.newItemTitle = '';
            $scope.newItemContent = '';

            $scope.itens.push(response.data)

        }, function errorCallBack(response) {
            console.log(response)
        })
    };

    $scope.deleteItem = function (noteId,index) {
        $http({
            method:'DELETE',
            url:'/notes/' + noteId
        }).then(function successCallBack(response){
            console.log(response)
            $scope.itens.splice(index,1)
        },function error(response) {
            console.log(response)
        })
    };



}]);