var app = angular.module('loja',['ngRoute']);

app.controller( 'primeiroController' , ['$scope',function ($scope) {

    $scope.user = {
        firstName: "João",
        lastName: "de Souza"
    };

    $scope.contador=0;

    $scope.addContador = function () {
        $scope.contador++
    };

    $scope.pessoas = [
        'joao',
        'maria',
        'ricardo',
        'marcel',
        'abc'
    ]

}]);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/teste',{controller:"listaController",templateUrl:"lista.html"})
        .otherwise({redirect:'/'});
});

app.controller('listaController',['$scope' , function ($scope) {
    console.log('teste')
}]);